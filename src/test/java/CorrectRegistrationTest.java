import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class CorrectRegistrationTest extends SeleniumBaseTest {

    @Test
    public void correctRegistrationTest() {
        String emailTxt = UUID.randomUUID().toString().substring(0, 8) + "@test.com";
        String passwordTxt = "Test1!";


        new LoginPage(driver)
                .goToNewAccout()
                .typeEmail(emailTxt)
                .typePassword(passwordTxt)
                .typeConfirmPassword(passwordTxt)
                .submitRegister()
                .assertWelcomeElementIsDisplayed();

    }
}
