import Utils.Utils;
import config.ConfigProvider;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest {

    protected WebDriver driver;
    public void takesScreenshot() throws Exception {
        TakesScreenshot takesScreenshot = ((TakesScreenshot) driver);
        String fileDestination = "resources/screens/SPC" + Utils.getDateAndTime() + Utils.generateRandomString(5) + ".png";
        File sourceFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        org.apache.commons.io.FileUtils.copyFile(sourceFile, new File(fileDestination));
    }

    @BeforeMethod
    public void setUp() {
        String chromedriver = "webdriver.chrome.driver";
        System.setProperty(chromedriver, new ConfigProvider().getDriver());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        driver.get(new ConfigProvider().getApplicationUrl());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        takesScreenshot();
        driver.quit();

    }
}
