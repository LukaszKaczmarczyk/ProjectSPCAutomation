import config.ConfigProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class AddNewProcessTest extends SeleniumBaseTest{

    @Test
    public void addProcessTest(){
        String processName = UUID.randomUUID().toString().substring(0,8);

        new LoginPage(driver)
                .typeEmail(new ConfigProvider().getApplicatinUser())
                .typePassword(new ConfigProvider().getAplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddNewProccess()
                .typeName(processName)
                .clickCreate()
                .goToDashboard()
                .assertProcessOnDashboard(processName);
    }
}
