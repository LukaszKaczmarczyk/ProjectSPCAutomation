import config.ConfigProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class ClickUrlMenuTest extends SeleniumBaseTest {

    @Test
    public void testClickUrlMenu() {

        new LoginPage(driver)
                .typeEmail(new ConfigProvider().getApplicatinUser())
                .typePassword(new ConfigProvider().getAplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessesUrl()
                .goToCharacteristics()
                .assertCharacteristicsUrl()
                .goToDashboard()
                .assertDashboardUrl();
    }
}

