import config.ConfigProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class CorrectLoginTest extends SeleniumBaseTest {

    @Test
    public void succesfullLogIn() {
        new LoginPage(driver)
                .typeEmail(new ConfigProvider().getApplicatinUser())
                .typePassword(new ConfigProvider().getAplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsDisplayed();
    }
}
