import Utils.Utils;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class IncorrectRegistrationIncompatiblePasswordsTest extends SeleniumBaseTest {

    @Test
    public void correctRegistrationTest() {
        String emailTxt = UUID.randomUUID().toString().substring(0, 8) + "@test.com";

        new LoginPage(driver)
                .goToNewAccout()
                .typeEmail(emailTxt)
                .typePassword(Utils.generateRandomString(6))
                .typeConfirmPassword(Utils.generateRandomString(6))
                .submitRegisterWithFailure()
                .assertErrorConfirmPassword("The password and confirmation password do not match.");
    }
}
