import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class IncorrectRegistrationPasswordTest extends SeleniumBaseTest {
    String emailTxt = UUID.randomUUID().toString().substring(0, 8) + "@test.com";

    @DataProvider
    public static Object[][] getWrongPasswords() {
        return new Object[][]{
                {"test", "The Password must be at least 6 and at max 100 characters long."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test11", "Passwords must have at least one non alphanumeric character."},
                {"Test!!", "Passwords must have at least one digit ('0'-'9')."}
        };
    }

    @Test(dataProvider = "getWrongPasswords")
    public void incorrectRegistrationPasswordRequirement(String passwordTxt, String expectedErrorMessage) {
        new LoginPage(driver)
                .goToNewAccout()
                .typeEmail(emailTxt)
                .typePassword(passwordTxt)
                .typeConfirmPassword(passwordTxt)
                .submitRegisterWithFailure()
                .assertValidationError(expectedErrorMessage);
    }
}
