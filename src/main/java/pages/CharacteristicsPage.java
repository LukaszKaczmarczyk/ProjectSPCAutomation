package pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    public CharacteristicsPage assertCharacteristicsUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("Characteristics"));
        return this;
    }
}
