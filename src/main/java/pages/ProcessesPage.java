package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessesPage extends HomePage {
    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "div.page-title > div > h3")
    private WebElement processesHeader;

    @FindBy(css = ".btn.btn-success")
    private WebElement addNewProcessBtn;

    public CreateNewProcessPage clickAddNewProccess() {
        addNewProcessBtn.click();
        return new CreateNewProcessPage(driver);
    }

    public ProcessesPage assertProcessesUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("Projects"));
        return this;
    }

    }
