package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class LoginPage extends SeleniumPage{
    public LoginPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    @FindBy(css = ".flash-message>strong")
    private WebElement logoutMessage;

    @FindBy(id = "Email-error")
    private WebElement errorLoginMessage;

    @FindBy(linkText = "Register as a new user?")
    private WebElement registerNewUserLink;


    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public RegistrationPage goToNewAccout(){
        registerNewUserLink.click();
        return new RegistrationPage(driver);
    }

    public LoginPage assertValidationError(String expError) {
        boolean doesErrorsExist = loginErrors
                .stream() // strimuje tworzy mape wszytstkich web elementow
                .map(validationError -> validationError.getText()) // mapuje wszystkie weblelementy i LAMBDA tworzy z webelementow Stringi
                .anyMatch(error -> error.equals(expError)); // sprawdza czy powyrzsze stringi odpowiadaja tekstowi z metody
        Assert.assertTrue(doesErrorsExist);
        return this;
    }

}
