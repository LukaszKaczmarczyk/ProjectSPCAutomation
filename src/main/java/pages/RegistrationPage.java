package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class RegistrationPage extends LoginPage{

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = ".btn.btn-success.submit.btn-block")
    private WebElement registerBtn;

    @FindBy(id = "Email-error")
    private WebElement registerLoginErrorMessage;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement confirmPasswordErrorMessage;


    public RegistrationPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public RegistrationPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public RegistrationPage typeConfirmPassword(String password) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitRegister() {
        registerBtn.click();
        return new HomePage(driver);
    }

    public RegistrationPage submitRegisterWithFailure() {
        registerBtn.click();
        return this;
    }

    public RegistrationPage assertErrorConfirmPassword(String expectedError) {
        Assert.assertTrue(confirmPasswordErrorMessage.isDisplayed());
        Assert.assertEquals(confirmPasswordErrorMessage.getText(), expectedError);
        return this;
    }
}
