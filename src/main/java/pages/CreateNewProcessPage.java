package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class CreateNewProcessPage extends HomePage {

    public CreateNewProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Name")
    private WebElement processNameField;

    @FindBy(css = ".btn-success")
    private WebElement createBtn;

    @FindBy(css = ".btn-primary")
    private WebElement backToListBtn;

    @FindBy(css = ".text-danger.field-validation-error")
    private WebElement addProcessErrorMsg;

    public CreateNewProcessPage typeName(String processName) {
        processNameField.clear();
        processNameField.sendKeys(processName);
        return this;
    }

    public ProcessesPage clickCreate() {
        createBtn.click();
        return new ProcessesPage(driver);
    }


}