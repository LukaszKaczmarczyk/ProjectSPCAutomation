package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class SeleniumPage {

    protected WebDriver driver;

    public SeleniumPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isElementPresent(WebElement webElement) {
        boolean isElementDisplayed = false;
        try {
            webElement.isDisplayed();
            isElementDisplayed = true;
        } catch (NoSuchElementException ex) {
        }
        return isElementDisplayed;
    }
}