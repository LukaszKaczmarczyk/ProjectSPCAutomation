package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class DashboardPage extends HomePage {


    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//p[text()='Characteristic']")
    public WebElement characteristicTxt;

    @FindBy(css = ".btn-success")
    public WebElement createYourFirstProcessBtn;

    private static final String GENERIC_PROCESS_ROW_XPATH = "//h2[text()='%s']";

    public DashboardPage assertDashboardUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("localhost:4444"));
        return this;
    }

    public DashboardPage assertProcessOnDashboard(String expectedName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expectedName);
        WebElement processRow = driver.findElement(By.xpath(processXpath));
        Assert.assertTrue(processRow.isDisplayed());
        Assert.assertEquals(processRow.getText(), expectedName);
        return this;
    }
}
