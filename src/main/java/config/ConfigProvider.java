package config;

import java.io.InputStream;
import java.util.Properties;

public class ConfigProvider {

    private Properties properties;

    public ConfigProvider() {
        properties = getProperties();
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            properties.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException(" cannot load properties file: " + e);
        }
        return properties;
    }

    public String getApplicationUrl() {
        return properties.getProperty("application.url");
    }
    public String getApplicatinUser(){
        return properties.getProperty("application.user");
    }
    public String getAplicationPassword(){
        return properties.getProperty("application.password");
    }
    public String getDriver(){
        return properties.getProperty("driver");
    }
}
